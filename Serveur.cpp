#include "Serveur.hpp"
#include "Error.hpp"
#include "Constantes.h"

#include <sstream>
#include <ios>
#include <ctime>
#include <iomanip>

using namespace tinyxml2_utils;
using namespace serveur_cst;

// functions
Serveur * Serveur::instance(tinyxml2::XMLElement * elem)
{
	static Serveur *serveur = new Serveur(elem);
	return serveur;
}

void Serveur::boot() {
	time_t temps;
	time(&temps);
	struct tm * boot_time = localtime(&temps);
	std::string message = M_BOOT+SPACE;
	message += asctime(boot_time);
	message.erase(message.size()-1);
	std::cout << message << std::endl;
	log_file_ << W_1+W_2+W_3+W_4+W_5;
	this->log_message(message+SPACE+M_LOCATION);
	this->log_message(M_INIT_OBJ);
}


Serveur::Serveur(tinyxml2::XMLElement* elem):
Process(get_elem_val(elem, "nom", false, "server")) {
	const char * log_file = (get_elem_val(elem, "fichier_log",
										  false, "server.log")).c_str();
	log_file_.open(log_file, std::ios::trunc);
	dat_file_.open(DATA.c_str(), std::ios::trunc);
	nb_controleurs_ = 0;
	this->boot();
}


void Serveur::transmit_data(double state, double phen, double val_ctrl) {
	std::vector<double> ligne;
	ligne.push_back(state);
	ligne.push_back(phen);
	ligne.push_back(val_ctrl);
	data_.push_back(ligne);
}


void Serveur::declare_controler(std::string nom) {
	using namespace std;
	if (!nb_controleurs_) {
		dat_file_ << '#';
	}
	
	ostringstream inside;
	
	inside << nom;
	inside << setw((MID_W_DATA - nom.size()/2.)-(COL_W/2.0)) << setfill(H_FILL)
		   << H_FILL;
	dat_file_ << setw(2*MID_W_DATA-1) << setfill(H_FILL) << inside.str();
	
	nb_controleurs_ ++;
	this->log_ctrl_init(nom);
}


void Serveur::log_ctrl_init(std::string nom) {
	this->log_message(M_INIT_CTRL+SPACE+nom);
	this->log_message(nom+SPACE+M_CONNECTED);
	this->log_message("Header for "+nom+" was added to data file ("+DATA+")");
}


bool Serveur::run(int tic) {
	using namespace std;

	dat_file_ << setw(TIC) << tic;
	for (int i(0); i<nb_controleurs_; i++) {
		if (i) {
			dat_file_ << setw(TIC-1) << setfill(V_FILL) << V_FILL;
		}
		for (size_t val(0); val < TRIPLET; val++) {
			dat_file_ << setw(COL_W)
			<< setprecision(PREC) << std::fixed << (data_.at(i)).at(val);
		}
	}
	
	dat_file_ << endl;
	
	for (size_t i=0; i<data_.size(); i++) {
		data_.at(i).clear();
	}
	data_.clear();
	
	return false;
}


void Serveur::log_message(std::string msg, bool err) {
	msg.insert(0, (err ? ERR : INFO));
	log_file_ << msg << std::endl;
}


void Serveur::log_statistics(int periode, std::string units, double fraction) {
	using namespace std;
	
	log_file_ << INFO+M_DURATION+SPACE << periode << SPACE << units << endl;
	
	periode*=fraction;
	
	if (units == "minutes") {
		periode*=60;
	} else if (units == "heures") {
		periode*=3600;
	}
	//	calcul d temps passé
	time_t temps;
	time(&temps);
	temps += periode;
	struct tm * boot_time = localtime(&temps);
	std::string message = M_SHUTDOWN+SPACE;
	message += asctime(boot_time);
	message.erase(message.size()-1);
	cout << message << endl;
	this->log_message(M_DOWN);
	this->log_message(message+SPACE+M_LOCATION);
}


void Serveur::initiate_header() {
	using namespace std;
	
	dat_file_ << endl << H_TIC;
	for (int i(0); i < nb_controleurs_; i++) {
		dat_file_ << setfill(V_FILL);
		if (i) {
			dat_file_ << setw(TIC-1) << setfill(V_FILL) << V_FILL;
		}
		dat_file_ << setw(COL_W) << H_STATE
		<< setw(COL_W) << H_PHEN << setw(COL_W) << H_CTRL;
	}
	dat_file_ << endl;
}

Serveur::~Serveur() {
	log_file_.close();
	dat_file_.close();
}
