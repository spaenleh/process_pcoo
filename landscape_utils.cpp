#include "landscape_utils.hpp"
#include "Etat.hpp"
#include "Phenomene.hpp"
#include "Sinus.hpp"
#include "Pulse.hpp"
#include "Controleur.hpp"
#include "CtrlOnOff.hpp"
#include "CtrlProp.hpp"
#include "Error.hpp"

using namespace std;
using namespace tinyxml2;
using namespace tinyxml2_utils;
using namespace landscape_cst;


std::string XML_FILE = "phase_2.xml";


XMLDocument* init_dom(int argc, char *argv[]) {
	if (argc >= ARG)
		XML_FILE = argv[IN_FILE];
	return create_dom(XML_FILE);
}


void create_landscape(Serveur *server, Simulator *simulator, XMLElement *p_root)
{	
	set_simulator(simulator);
	set_server(server);
	
	//	Boucle d'initialisation sur les processus
	for (XMLElement* p_zone = get_elem(p_root, "Zone");
		 p_zone;
		 p_zone = p_zone->NextSiblingElement("Zone"))
	{
		cout << "Création d'un triplet." << endl;
		try {
			Etat* etat = new Etat(get_elem(p_zone, "Etat"));
			
			XMLElement* p_ctrl = get_elem(p_zone, "Controleur");
			Controleur* controleur;
			if (get_attr_val(p_ctrl, "regulation") == "on-off") {
				controleur = new CtrlOnOff(server, etat, p_ctrl);
			} else {
				controleur = new CtrlProp(server, etat, p_ctrl);
			}
			
			XMLElement* p_phen = get_elem(p_zone, "Phenomene");
			Phenomene* phenomene;
			if (get_attr_val(p_phen, "signal") == "sinus") {
				phenomene = new Sinus(etat, p_phen);
			} else {
				phenomene = new Pulse(etat, p_phen);
			}
			simulator->add_Process(phenomene);
			simulator->add_Process(controleur);
			simulator->add_Process(etat);
		} catch (string& msg) {
			Error e = {true, msg};
			error_occured(e);
		}
	}
	cout << "Tous les triplets ont été crées." << endl;
	server->initiate_header();
	simulator->add_Process(server);
}


void end_of_simulation(Serveur* server, Simulator* simulator) {
	int periode = simulator->get_simul_periode();
	std::string unit = simulator->get_periode_units();
	double fraction = simulator->get_time_fraction();
	server->log_statistics(periode, unit, fraction);
}
