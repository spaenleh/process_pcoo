#include "Pulse.hpp"

using namespace tinyxml2_utils;


Pulse::Pulse(Etat* p_etat, tinyxml2::XMLElement* elem): Phenomene(p_etat, elem),
tdel_(get_elem_dbl(elem, "tDel", false)) {
	try {
		vlow_ = get_elem_dbl(elem, "vLow");
		vhigh_ = get_elem_dbl(elem, "vHigh");
		period_ = get_elem_dbl(elem, "periode");
		trise_ = get_elem_dbl(elem, "tRise");
		tfall_ = get_elem_dbl(elem, "tFall");
		pwidth_ = get_elem_dbl(elem, "pWidth");
		if (period_ <= 0) {
			Error e = {true, "Periode"+error_msg::POSITIV};
			throw e;
		}
		
		if (trise_ <= 0) {
			Error e = {true, "tRise"+error_msg::POSITIV};
			throw e;
		}
		
		if (tfall_ <= 0) {
			Error e = {true, "tFall"+error_msg::POSITIV};
			throw e;
		}
		
		if (pwidth_ <= 0) {
			Error e = {true, "pWidth"+error_msg::POSITIV};
			throw e;
		}
	} catch (std::string &msg) {
		Error e = {true, error_msg::MISSING_VALUE+msg};
		error_occured(e);
	} catch (Error &e) {
		error_occured(e);
	}
}


bool Pulse::run(int tic) {
	double val_phen;
	double x = fmod(tic, period_) - tdel_;
	if (x < 0) {
		val_phen = vlow_;
	} else if (x < trise_){
		val_phen = x * (vhigh_-vlow_)/trise_ + vlow_;
	} else if (x < trise_ + pwidth_) {
		val_phen = vhigh_;
	} else if (x < trise_ + pwidth_ + tfall_) {
		double a = (vhigh_-vlow_)/tfall_;
		double b = a*(trise_ + pwidth_ + tfall_) + vlow_;
		val_phen = -a*x + b;
	} else {
		val_phen = vlow_;
	}
	double x1 = static_cast<double>(rand())/RAND_MAX;
	double x2 = static_cast<double>(rand())/RAND_MAX;
	double y = sqrt(-2*log(x1))*cos(2*M_PI*x2);
	val_phen += y*sigma_;
	p_etat_->set_phen(val_phen);
	return true;
}


Pulse::~Pulse() {}
