#include "Controleur.hpp"

using namespace tinyxml2_utils;
using namespace tinyxml2;



Controleur::Controleur(Serveur* p_server, Etat* p_etat, XMLElement* elem):
Process(get_elem_val(elem, "nom", false, "controleur")) {
	try {
		if (!p_server)
			throw "Server";
		p_server_ = p_server;
		if (!p_etat)
			throw "Etat";
		p_etat_ = p_etat;
	} catch (std::string& who) {
		Error e = {true, error_msg::POINTER+who};
		error_occured(e);
	}
	p_server_->declare_controler(nom_);
}


Controleur::~Controleur() {}
