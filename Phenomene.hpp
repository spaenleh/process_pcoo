/*!	\file		Phenomene.hpp
 *  \brief		Class Phenomene
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	29/11/2018
 *
 *	
 */

#ifndef Phenomene_hpp
#define Phenomene_hpp

#define _USE_MATH_DEFINES
#include <cmath>

#include "Process.hpp"
#include "Etat.hpp"
#include "Error.hpp"


/*! \brief	Class Phenomene
*
*  Source de valeurs reelles en fonction du temps qui affecte l'Etat
*/
class Phenomene: public Process {
public:

	// Phenomene
	/*!
	 *    Default Constructor
	 *    \param   p_etat	 Pointer to state actor of the triplet
	 *	  \param   elem    Pointer to parent XML element
	 */
	Phenomene(Etat* p_etat, tinyxml2::XMLElement* elem);

	// run
	/*!
	*    Generate the phenomene with the information of the perturbation
	*    \param   tic		 Actual tic of the simulation
	*/
	virtual bool run(int tic)=0;

	// set_p_etat
	/*!
	 *    Set the phenomene value to the given state pointer
	 *	  \param   p_etat   Pointer to the triplet state
	 */
	void set_p_etat(Etat* p_etat);

	// ~Phenomene
	/*! Destructor */
	virtual ~Phenomene();
	
protected:
	/*! Pointeur sur l'etat*/
	Etat* p_etat_;

	/*! Racine de la variance */
	double sigma_;

};


#endif /* Phenomene_hpp */
