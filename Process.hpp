/*!	\file		Process.hpp
 *  \brief		Abstract Class Process
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	15/12/2018
 *
 *	Describes the model for a %Process therefor is an abstract class
 */

#ifndef Process_hpp
#define Process_hpp

#include <iostream>
#include "Constantes.h"
#include "xml_utils/xml_utils.h"


/*! \brief	Class Process
 *
 *	Classe Abstraite Modèle pour utiliser le polymorphisme
 */
class Process {
public:
	// Process
	/*! Constructor */
	Process(std::string nom):nom_(nom) {};

	// Process
	/*!	  Virtual parent function run
	 *	  \param   tic	Tic of the 
	 */
	virtual bool run(int tic) =0;

	// ~Process
	/*! Destructor */
	virtual ~Process() {};
	
protected:
	
	/*! Nom du Processus */
	std::string nom_;
};

#endif /* Process_hpp */
