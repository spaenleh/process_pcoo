/*!	\file		Error.hpp
 *  \brief		Class Error
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	29/11/2018
 *
 *	Basic Implementation
 */

#ifndef Error_hpp
#define Error_hpp

#include "Serveur.hpp"
#include "Simulator.hpp"

struct Error {
	bool fatal_;
	std::string msg_;
};

void set_server(Serveur* server);

void set_simulator(Simulator* simulator);

void error_occured(Error &e);

#endif /* Error_hpp */
