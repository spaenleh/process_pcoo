/*!	\file		landscape_utils.hpp
 *  \brief		SubModule to create the objects of the landscape
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	15/12/2018
 */

#ifndef LANDSCAPE_UTILS_HPP
#define LANDSCAPE_UTILS_HPP

#include "xml_utils/xml_utils.h"
#include "Serveur.hpp"
#include "Simulator.hpp"


/*!
 *	Initiate the DOM from an XML file
 *
 *    \param   argc		Number of arguments given.
 *    \param   argv		Arguments given at runtime.
 *
 *    \return  The XMLDocument contained in the XML file
 */
tinyxml2::XMLDocument* init_dom(int argc, char *argv[]);

/*!
 *	Create all the objects from the p_root pointer given
 *
 *    \param	server			Pointer to object Server.
 *    \param	simulator		Pointer to object Simulator.
 *	  \param	p_root			Root of the DOM to extract objects from.
 */
void create_landscape(Serveur *server, Simulator *simulator, tinyxml2::XMLElement *p_root);

/*!
 *	At the end of the simulation gives the usefull informations to server to log into its file
 *
 *    \param	server			Pointer to object Server.
 *    \param	simulator		Pointer to object Simulator.
 */
void end_of_simulation(Serveur* server, Simulator* simulator);

#endif /* LANDSCAPE_UTILS_HPP */
