/*!	\file		Etat.hpp
 *  \brief		Class Etat
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 *
 *	Basic Implementation
 */

#ifndef Etat_h
#define Etat_h

#include "Process.hpp"
#include "Error.hpp"

/*!	\brief	Class Etat
*
*	Calcule la valeur de l'etat au temps t
*/
class Etat: public Process {

public:

	// Etat
	/*!
	 *    Constructor
	 *	  \param   elem      Pointer to parent XML element
	 */
	Etat(tinyxml2::XMLElement* elem);

	// get_phen
	/*! Return the value of the phenomene. */
	double get_phen();

	// get_etat
	/*! Return the value of the state. */
	double get_etat();

	// set_ctrl
	/*!
	 *    Set the given control value.
	 *	  \param   ctrl    Control value
	 */
	void set_ctrl(double ctrl);

	// set_phen
	/*!
	 *    Set the phenomene value.
	 *	  \param   val_phen   Phenomene value
	 */
	void set_phen(double val_phen);

	// run
	/*!
	 *    Calculate the new state and return true.
	 *    \param   tic		 Actual tic of the simulation
	 */
	virtual bool run(int tic);

	// ~Etat
	/*! Destructor */
	virtual ~Etat();

private:

	/*! Valeur de l'etat */
	double etat_;

	/*! Facteur d'influence phenomene*/
	double iphen_;

	/*! Facteur d'influence controle*/
	double ictrl_;

	/*! Valeur regulee generee par le controleur au temps t */
	double val_ctrl_;

	/*! Valeur du phenomene au temps t */
	double val_phen_;
};


#endif /* Etat_h*/
