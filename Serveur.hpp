/*!	\file		Serveur.hpp
 *  \brief		Class Serveur Singleton
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 *
 *	Basic Implementation
 */

#ifndef Serveur_hpp
#define Serveur_hpp

#include <vector>
#include <fstream>
#include "Process.hpp"


/*!	\brief	Class Serveur
*
*	S'occupe de la gestion des fichiers de donnees (journal et gnuplot)
*/
class Serveur: public Process {
public:

	//singleton 
	static Serveur* instance(tinyxml2::XMLElement* elem);

	// declare_controler
	/*!
	 *    Declare a controler and add it to the files vector
	 *	  \param   nom      Name of the controler
	 */
	void declare_controler(std::string nom);
	
	// initiate_header
	/*!
	 *    Initiates the header in the data file to put the right collums
	 */
	void initiate_header();

	// transmit_data
	/*!
	 *   Initialize the vector data with the given parameter values
	 *	 \param   state		   State value
	 *   \param   phen	       Phenomene value
	 *	 \param   val_ctrl     Control value given by Controleur
	 */
	void transmit_data(double state, double phen, double val_ctrl);

	// run
	/*!
	 *    Write in the files for the given tic of the simulation
	 *    \param   tic		 Actual tic of the simulation
	 */
	virtual bool run(int tic);

	// log_message
	/*!
	 *    Calculate the new state and return true
	 *    \param   msg	 Message to insert (error or info)
	 *    \param   err   Decides if the message is an error or an information
	 */
	void log_message(std::string msg, bool err=false);
	
	// log_statistics
	/*!
	 *    
	 *    \param   periode		Duration of the simulation
	 *    \param   units		Unit of the periode
	 *	  \param   fraction		Fraction represented by the tic
	 */
	void log_statistics(int periode, std::string units, double fraction);

private:
	/*!
	*    Constructor
	*	 \param   elem      Pointer to parent XML element
	*/
	Serveur(tinyxml2::XMLElement* elem);
	
	/*! Destructor */
	virtual ~Serveur();

	/*! Emulate the physical boot of the server */
	void boot();
	
	/*! Initiates the controlers connection */
	void log_ctrl_init(std::string nom);
	
	/*! File for the Journal */
	std::ofstream log_file_;
	
	/*! File where the data is stored */
	std::ofstream dat_file_;
	
	/*! Data structure */
	std::vector< std::vector<double> > data_;
	
	/*! Holds the number of controlers connected */
	int nb_controleurs_;
};

#endif /* Serveur_hpp */
