#include "Phenomene.hpp"

using namespace tinyxml2_utils;

Phenomene::Phenomene(Etat* p_etat, tinyxml2::XMLElement* elem):
Process(get_elem_val(elem, "nom")),
sigma_(sqrt(get_elem_dbl(elem, "variance", false, 0.0))) {
	set_p_etat(p_etat);
}//	Phenomene() xml


void Phenomene::set_p_etat(Etat* p_etat) {
	try {
		if (p_etat) {
			p_etat_ = p_etat;
		} else {
			Error e = {true, error_msg::POINTER+"for State in Phenomene."};
			throw e;
		}
	} catch (Error& e) {
		error_occured(e);
	}
}


Phenomene::~Phenomene() {}
