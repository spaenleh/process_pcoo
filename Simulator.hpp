/*!	\file		Simulator.hpp
 *  \brief		Class Simulator
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 */

#ifndef Simulator_hpp
#define Simulator_hpp

#include <vector>
#include "Process.hpp"

/*! \brief	Class Simulator
 *
 *	Classe Simulator pour la simulation du paysage
 */
class Simulator {
public:

	// Simulator
	/*!
	*    Constructor
	*    \param   elem Pointer to parent XML element
	*/
	Simulator(tinyxml2::XMLElement* elem);

	//Shutdown
	/*! Delete all the processes */
	void shutdown();

	// Simulate
	/*! Run the simulation for a given period */
	void simulate();

	// add_Process
	/*!
	*   Add a new Process to the vector processes
	*    \param  new_process  Process Pointer to the new process
	*/
	void add_Process(Process* new_process);

	// get_simul_periode
	/*! Return the period of simulation */
	int get_simul_periode();

	// get_periode_units
	/*! Return the unit of the period */
	std::string get_periode_units();

	// get_periode_units
	/*! Return the unit of the period */
	double get_time_fraction();
	
	// get_tic
	/*! Return the actual tic */
	int get_tic();

	// ~Simulator
	/*! Destructor */
	virtual ~Simulator();
	
private:

	/*! Vector de Process*/
	std::vector<Process*> processes_;

	/*! Tic de la simulation */
	int tic_;

	/*! Duree de la simulation */
	int simul_periode_;

	/*! Unite de la dure de simulation */
	std::string periode_unit_;
	
	/*! Representation fractionaire de la periode */
	double time_fraction_;
};

#endif /* Simulator_hpp */
