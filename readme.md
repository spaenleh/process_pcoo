# PCOO_Process_Control

Projet de programmation orienté objet ayant pour objectif de simuler un paysage
contenant différentes perturbations gérées par un système de contrôle régulant
l'environment

## Auteurs :
- Nicolas Hoischen
- Basile Spaenlehauer

## Phase de développement :

Le projet est dans son état final.

Le paysage inclut la représentation du triplet chambre
(source sinusoidale non saturée) et du triplet aquarium (source pulsée).
L'état prends en compte les valeurs de phénomène et de controle via les influences
définies par `iphen` et `ictrl`.

Le Serveur est responsable de la sauvegarde des données de simulation dans le
fichier portant le nom du contrôleur associé.

## Utilisation :

Le nom du fichier de sauvegarde du journal du serveur est spécifié dans le
fichier de configuration XML.
Les bibliothèques utilisées pour XML sont `tinyxml2` et `xml_utils`.

Le nom du fichier de configuration XML est par défault `phase_2.xml` mais il
peut être spécifié sur la ligne de commande au moment de l'execution.

`./projet.x config.xml`

Tous les objets disposent de deux constructeurs distincts : l'un est un
constructeur xml qui prend des `XMLElements` pour s'initialiser l'autre est un
constructeur qui peut être utilisé pour initialiser les objets dans le main.



 `main.cpp` est au point et reçoit un fichier XML duquel on extrait les données.
 `Process` classe mère abstraite permet de traiter tous les acteurs comme des
 Processus du point de vue de `Simulator`.


## Mention spéciale :

Cette documentation est rédigée en utilisant le style **Markdown**
