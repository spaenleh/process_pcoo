/*!	\file		Controleur.hpp
 *  \brief		Class Contorleur
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 */

#ifndef CONTROLEUR_H
#define CONTROLEUR_H

#include "Process.hpp"
#include "Serveur.hpp"
#include "Etat.hpp"
#include "Error.hpp"
#include <string>
#include <vector>

 /*! \brief	Class Controleur
  *
  *	Calcule la valeur de controle à partir des valeurs d'etats et
  * de phenomene
  */
class Controleur: public Process {
public:
	
	// Controleur
	/*!
	 *    Constructor
	 *    \param   p_server  Pointer to the unique Serveur
	 *    \param   p_etat	 Pointer to the state actor of the triplet
	 *	  \param   elem      Pointer to parent XML element
	 */
	Controleur(Serveur* p_server, Etat* p_etat, tinyxml2::XMLElement* elem);

	// run
	/*!
	 *    Calculate the new control value and interact with the server
	 *    \param   tic		 Actual tic of the simulation
	 */
	virtual bool run(int tic)=0;

	// ~Controleur
	/*! Destructor */
	virtual ~Controleur();
	
protected:
	
	/*! Pointeur sur le serveur */
	Serveur* p_server_;

	/*! Pointeur sur l'etat */
	Etat* p_etat_;
};

#endif /* CONTROLEUR_H */
