#include "Sinus.hpp"

using namespace tinyxml2_utils;


Sinus::Sinus(Etat* p_etat, tinyxml2::XMLElement* elem): Phenomene(p_etat, elem),
offs_(get_elem_dbl(elem, "offset", false, 0.0)),
phase_(get_elem_dbl(elem, "phase", false, 0.0)),
sat_max_(get_elem_dbl(elem, "satMax", false, HUGE_VAL)),
sat_min_(get_elem_dbl(elem, "satMin", false, -HUGE_VAL))
{
	periode_ = get_elem_dbl(elem, "periode");
	ampl_ = get_elem_dbl(elem, "amplitude");
	if (periode_ <= 0) {
		Error e = {true, "Periode"+error_msg::POSITIV};
		error_occured(e);
	}
}


bool Sinus::run(int tic) {
	double val_phen = offs_;
	val_phen+=ampl_*(sin((2*M_PI*(tic + phase_))/static_cast<double>(periode_)));

	double x1 = static_cast<double>(rand())/RAND_MAX;
	double x2 = static_cast<double>(rand())/RAND_MAX;
	double y = sqrt(-2*log(x1))*cos(2*M_PI*x2);
	val_phen += y*sigma_;
	
	if (val_phen > sat_max_)
		val_phen = sat_max_;
	if (val_phen < sat_min_)
		val_phen = sat_min_;
	
	p_etat_->set_phen(val_phen);
	return true;
}


Sinus::~Sinus() {}
