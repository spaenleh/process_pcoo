#include "Simulator.hpp"
#include <math.h>
#include "Constantes.h"

using namespace tinyxml2_utils;


Simulator::Simulator(tinyxml2::XMLElement* elem): tic_(0),
			simul_periode_(get_attr_int(elem, "duration", false, 1)),
			periode_unit_(get_attr_val(elem, "unit", false, "sec")),
			time_fraction_(get_attr_dbl(elem, "fraction", false, 1)) {}


void Simulator::shutdown() {
	for (size_t i=0; i < processes_.size(); i++)
		delete processes_.at(i);
	processes_.clear();
}


void Simulator::add_Process(Process* new_process) {
	processes_.push_back(new_process);
}


void Simulator::simulate() {

	using namespace simulator_cst;
	std::cout << M_PROGRESS;
	for (tic_=0; tic_ < simul_periode_; tic_++) {
		for (size_t j=0; j<processes_.size(); j++)
			processes_.at(j)->run(tic_);
		
		double pourcentage = (static_cast<double> (tic_) / simul_periode_) * 100;

		if (fmod(pourcentage, STEP_POURCENTAGE) <= EPSILON)
			std::cout << static_cast<int>(pourcentage) << POURCENTAGE;
	}
	std::cout << "100%" << std::endl;
}


int Simulator::get_simul_periode() { return simul_periode_; }


std::string Simulator::get_periode_units() { return periode_unit_; }


double Simulator::get_time_fraction() { return time_fraction_; }


int Simulator::get_tic() { return tic_; }


Simulator::~Simulator() {}
