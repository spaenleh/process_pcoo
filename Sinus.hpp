/*!	\file		Serveur.hpp
 *  \brief		Class Sinus
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 *
 *	Basic Implementation
 */

#ifndef SINUS_HPP
#define SINUS_HPP

#include "Phenomene.hpp"

/*! \brief	Class Sinus
*
*  Source de valeurs reelles affectant l'etat du type sinus
*/
class Sinus : public Phenomene {
public:

	// Sinus
	/*!
	 *    Constructor
	 *    \param   p_etat	 Pointer to state actor of the triplet
	 *	  \param   elem      Pointer to parent XML element
	 */
	Sinus(Etat* p_etat, tinyxml2::XMLElement* elem);
	
	// run
	/*!
	*    Generate the sinus with its properties
	*    \param   tic		 Actual tic of the simulation
	*/
	virtual bool run(int tic);

	// ~Sinus
	/*! Destructor */
	~Sinus();
	
private:
	
	/*! Offset */
	double offs_;

	/*! Amplitude */
	double ampl_;

	/*! Phase */
	long int phase_;

	/*! Periode */
	long int periode_;

	/*! Saturation maximum */
	double sat_max_;

	/*! Saturation minimum */
	double sat_min_;
};


#endif /* SINUS_HPP */
