/*!	\file		Constantes.hpp
 *  \brief		Constantes Namespaces List
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 */



#ifndef CONSTANTES_HPP
#define CONSTANTES_HPP

namespace error_msg
{
	const std::string FATAL_ERROR("Arret fatal de la simulation au tic : ");
	const std::string ERROR("ERROR : ");
	const std::string MISSING_VALUE("Missing Value : ");
	const std::string POINTER("Pointer is NULL : ");
	const std::string POSITIV(" must be > 0.");
}

namespace simulator_cst
{
	const int STEP_POURCENTAGE(10);
	const double EPSILON(1e-6);
	const std::string M_PROGRESS("Progression de la simulation : ");
	const std::string POURCENTAGE("%    ");
}

namespace landscape_cst
{
	const int IN_FILE(1);
	const int ARG(2);
}

namespace serveur_cst
{
	//	Constants
	const int TRIPLET(3);
	const int T_WARN(8);
	const int PREC(3);
	const int HEAD_W(20);
	const int COL_W(10);
	const int TIC(6);
	
	const int MID_W_DATA((TIC+3*COL_W)/2); // 15
	const char H_FILL(' ');
	const char V_FILL(' ');
	const std::string H_TIC("# Tick");
	const std::string H_STATE("State");
	const std::string H_PHEN("Phen");
	const std::string H_CTRL("Ctrl");

	const std::string INFO("-i- ");
	const std::string ERR("-e- ");
	const std::string WARN("-w- ");
	const std::string SPACE(" ");
	const std::string HEAD_STAT("STATISTICS");
	const std::string TICK("Tick ");
	const std::string M_BOOT("Server Boot :");
	const std::string M_LOCATION("in Lausanne VD");
	const std::string M_INIT_OBJ("... Initializing Objects ...");
	const std::string M_INIT_CTRL("Initializing connection to");
	const std::string M_CONNECTED("is now connected");
	const std::string M_DURATION("Server uptime :");
	const std::string M_SHUTDOWN("Server Shutdown :");
	const std::string M_DOWN("... Shutting down ...");
	
	const std::string DATA("data.txt");
	
	const std::string W_1("   _____                              ____  __________  ____\n");
	const std::string W_2("  / ___/___  ______   _____  _____   / __ \\/ ____/ __ \\/ __ \\\n");
	const std::string W_3("  \\__ \\/ _ \\/ ___/ | / / _ \\/ ___/  / /_/ / /   / / / / / / /\n");
	const std::string W_4(" ___/ /  __/ /   | |/ /  __/ /     / ____/ /___/ /_/ / /_/ /\n");
	const std::string W_5("/____/\\___/_/    |___/\\___/_/     /_/    \\____/\\____/\\____/\n\n");
	
} //Fin du namespace


#endif

