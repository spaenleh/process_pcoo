/*!	\file		CtrlOnOff.hpp
 *  \brief		Class CtrlOnOff
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 */

#ifndef CtrlOnOff_hpp
#define CtrlOnOff_hpp

#include "Controleur.hpp"

/*! \brief	Class CtrlOnOff
*
*	Calcule la valeur de controle du type tout ou rien
*/
class CtrlOnOff: public Controleur {
public:
	// CtrlOnOff
	/*!
	 *    Constructor
	 *	  \param   p_server  Pointer to the unique server
	 *    \param   p_etat	 Pointer to state actor of the triplet
	 *	  \param   elem    Pointer to parent XML element
	 */
	CtrlOnOff(Serveur* p_server, Etat* p_etat, tinyxml2::XMLElement* elem);

	// run
	/*!
	 *    Calculate the new On-Off type value and transmit it to the server
	 *    \param   tic		 Actual tic of the simulation
	 */
	virtual bool run(int tic);

	// ~CtrlOnOff
	/*! Destructor */
	virtual ~CtrlOnOff();
	
private:
	
	/*! Valeur de seuil maximum */
	double vthrmax_;

	/*! Valeur de seuil minimum */
	double vthrmin_;

	/*! Valeur controlee maximum*/
	double vctrlmax_;

	/*! Valeur controlee minimum*/
	double vctrlmin_;
};

#endif /* CtrlOnOff_hpp */
