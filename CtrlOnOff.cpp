#include "CtrlOnOff.hpp"

using namespace tinyxml2_utils;
using namespace tinyxml2;


CtrlOnOff::CtrlOnOff(Serveur* p_server, Etat* p_etat, XMLElement* elem):
Controleur(p_server, p_etat, elem),
vthrmax_(get_elem_dbl(elem, "Seuil_max")),
vthrmin_(get_elem_dbl(elem, "Seuil_min")),
vctrlmax_(get_elem_dbl(elem, "Val_max")),
vctrlmin_(get_elem_dbl(elem, "Val_min")) {}


bool CtrlOnOff::run(int tic) {
	double etat = p_etat_->get_etat();
	double val_ctrl;
	
	if (etat > vthrmax_)
		val_ctrl = vctrlmin_;
	else if (etat < vthrmin_)
		val_ctrl = vctrlmax_;
	else
		val_ctrl = etat;
	
	p_etat_->set_ctrl(val_ctrl);
	p_server_->transmit_data(etat, p_etat_->get_phen(), val_ctrl);
	return true;
}


CtrlOnOff::~CtrlOnOff() {}
