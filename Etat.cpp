#include "Etat.hpp"

using namespace tinyxml2_utils;

Etat::Etat(tinyxml2::XMLElement* elem):Process(get_elem_val(elem, "nom")),
etat_(get_elem_dbl(elem, "etat_init")), iphen_(get_elem_dbl(elem, "Iphenomene")),
ictrl_(get_elem_dbl(elem, "Icontroleur")) {}


double Etat::get_phen() { return val_phen_; }


double Etat::get_etat() { return etat_; }


void Etat::set_ctrl(double ctrl) { val_ctrl_ = ctrl; }


void Etat::set_phen(double val_phen) { val_phen_ = val_phen; }


bool Etat::run(int tic) {
	etat_ = etat_ + (val_phen_ - etat_) * iphen_ + (val_ctrl_ - etat_) * ictrl_ ;
	return true;
}


Etat::~Etat() {}
