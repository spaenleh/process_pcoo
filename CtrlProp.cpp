#include "CtrlProp.hpp"

using namespace tinyxml2_utils;

CtrlProp::CtrlProp(Serveur* p_server, Etat* p_etat, tinyxml2::XMLElement* elem):
Controleur(p_server, p_etat, elem), gain_(get_elem_dbl(elem, "gain", false, 1.)){
	 try {
		 setpt_ = get_elem_dbl(elem, "Val_consigne");
	 } catch (std::string& msg) {
		 Error e = {true, error_msg::MISSING_VALUE+msg};
	 }
 }


bool CtrlProp::run(int tic) {
	double etat = p_etat_->get_etat();
	
	double val_ctrl = etat - gain_ * (etat - setpt_);
	
	p_etat_->set_ctrl(val_ctrl);
	p_server_->transmit_data(etat, p_etat_->get_phen(), val_ctrl);
	return true;
}


CtrlProp::~CtrlProp() {}
