/*!	\file		main.cpp
 *  \brief		Main file for Project Process_PCOO
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	15/12/2018
 */

#include <iostream>

#include "Serveur.hpp"
#include "Simulator.hpp"

#include "Error.hpp"
#include "xml_utils/xml_utils.h"
#include "landscape_utils.hpp"

using namespace std;
using namespace tinyxml2;
using namespace tinyxml2_utils;

// main
/*!
 * Main function defines the Landscape of the application and starts
 * the simulation
 *
 *    \param   argc		Number of arguments given.
 *    \param   argv		Arguments given at runtime.
 *
 *    \return  The State of execution.
 */
int main (int argc, char *argv[]) {
	
	try {
		//	Prends le bloc paysage
		cout << "Création de l'arbre de DOM a partir du XML." << endl;
		XMLElement* p_root = init_dom(argc, argv)->FirstChildElement("Paysage");
		Serveur* server = Serveur::instance(get_elem(p_root, "Serveur"));
		Simulator* simulator = new Simulator(p_root);
		
		cout << "Création du paysage a partir du DOM." << endl;
		//	Crée les triplets
		create_landscape(server, simulator, p_root);
		
		cout << "Lancement de la simulation." << endl;
		simulator->simulate();
		cout << "Fin de la simulation." << endl;
		
		cout << "Écriture des statistiques." << endl;
		end_of_simulation(server, simulator);
		cout << "Fin du programme." << endl;
	} catch (string &msg) {
		Error e = {true, msg};
		error_occured(e);
	}
	
	return 0;
}
