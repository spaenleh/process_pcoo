/*!	\file		Pulse.hpp
 *  \brief		Class Pulse
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	15/12/2018
 *
 *	Basic Implementation
 */

#ifndef PULSE_HPP
#define PULSE_HPP

#include "Phenomene.hpp"

/*! \brief	Class Pulse
*
*  Source de valeurs reelles affectant l'etat du type pulse
*/
class Pulse : public Phenomene {
public:
	// Pulse
	/*!
	 *    Constructor
	 *    \param   p_etat	 Pointer to state actor of the triplet
	 *	  \param   elem      Pointer to parent XML element
	 */
	Pulse(Etat* p_etat, tinyxml2::XMLElement* elem);

	// run
	/*!
	*    Generate the pulse phenomene with its properties
	*    \param   tic		 Actual tic of the simulation
	*/
	virtual bool run(int tic);

	// ~Pulse
	/*! Destructor */
	~Pulse();
	
private:

	/*! Valeur basse */
	double vlow_;

	/*! Valeur haute */
	double vhigh_;

	/*! Delai initial */
	long int tdel_;

	/*! Periode */
	long int period_;

	/*! Temps de montee */
	long int trise_;

	/*! Temps de descente */
	long int tfall_;

	/*! Duree a valeur haute */
	long int pwidth_;
};
#endif /* PULSE_HPP */
