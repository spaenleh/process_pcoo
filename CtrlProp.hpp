/*!	\file		CtrlProp.hpp
 *  \brief		Class CtrlProp
 *  \author		Basile Spaenlehauer
 *  \author 	Nicolas Hoischen
 *  \date   	17/12/2018
 */

#ifndef CtrlProp_hpp
#define CtrlProp_hpp

#include <iostream>
#include "Controleur.hpp"

/*! \brief	Class CtrlProp
*
*	Calcule la valeur de controle du type proportionnel
*/
class CtrlProp: public Controleur {
public:
	// CtrlProp
	/*!
	 *    Constructor
	 *	  \param   p_server  Pointer to the unique server
	 *    \param   p_etat	 Pointer to state actor of the triplet
	 *	  \param   elem    Pointer to parent XML element
	 */
	CtrlProp(Serveur* p_server, Etat* p_etat, tinyxml2::XMLElement* elem);

	// run
	/*!
	 *  Calculate the new Proportional type value and transmit it to the server
	 *  \param   tic		 Actual tic of the simulation
	 */
	virtual bool run(int tic);

	// ~CtrlProp
	/*! Destructor */
	virtual ~CtrlProp();
	
private:

	/*! Valeur de consigne */
	double setpt_;

	/*! Gain du controleur */
	double gain_;
};

#endif /* CtrlProp_hpp */
