#include "Error.hpp"

#include <sstream>

//	Static members
Simulator* simulator_ = NULL;
Serveur* server_ = NULL;
using namespace error_msg;

void set_simulator(Simulator* simulator) { simulator_ = simulator; }


void set_server(Serveur* server) { server_ = server; }


void error_occured(Error &e) {
	using namespace std;
	cout << ERROR << e.msg_ << std::endl;
	if (simulator_ && e.fatal_) {
		ostringstream smsg;
		smsg << FATAL_ERROR << simulator_->get_tic();
		std::string msg = smsg.str();
		server_->log_message(msg, true);
		simulator_->shutdown();
	} else if (e.fatal_)
		exit(EXIT_FAILURE);
}
